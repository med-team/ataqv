From: Michael R. Crusoe <michael.crusoe@gmail.com>
Subject: convert scripts to Python3
--- a/src/scripts/mkarv
+++ b/src/scripts/mkarv
@@ -1,6 +1,6 @@
-#!/usr/bin/env python
+#!/usr/bin/python3
+
 
-from __future__ import print_function
 
 import argparse
 import collections
@@ -301,10 +301,10 @@ def add_fraction_of_reads(fragment_lengt
     Given a map of fragment lengths to read counts, calculate the fraction of all reads represented at each fragment length.
     """
     total_reads = 0.0
-    for fragment_length, count in fragment_length_counts.items():
+    for fragment_length, count in list(fragment_length_counts.items()):
         total_reads += count
 
-    for fragment_length, count in fragment_length_counts.items():
+    for fragment_length, count in list(fragment_length_counts.items()):
         if total_reads < 1:
             fraction = 0
         else:
@@ -322,7 +322,7 @@ def prepare_fragment_length_counts(fragm
     """
 
     # adjust the distribution to the requested maximum fragment length
-    prepared_counts = collections.defaultdict(long)
+    prepared_counts = collections.defaultdict(int)
     for fragment_length, count, fraction_of_total_reads in fragment_length_counts:
         if fragment_length <= max_fragment_length:
             prepared_counts[fragment_length] = count
@@ -336,16 +336,16 @@ def construct_fragment_length_reference(
     """
     Construct a reference fragment length density distribution from all metrics in `data`.
     """
-    metrics = data['metrics'].values()
+    metrics = list(data['metrics'].values())
     metrics_count = len(metrics)
 
-    reference_distribution = collections.defaultdict(long)
+    reference_distribution = collections.defaultdict(int)
     for m in metrics:
-        for fragment_length, [count, fraction_of_total_reads] in m['fragment_length_counts'].items():
+        for fragment_length, [count, fraction_of_total_reads] in list(m['fragment_length_counts'].items()):
             if fragment_length <= max_fragment_length:
                 reference_distribution[fragment_length] += count
 
-    for fragment_length, count in reference_distribution.items():
+    for fragment_length, count in list(reference_distribution.items()):
         reference_distribution[fragment_length] /= metrics_count
 
     add_fraction_of_reads(reference_distribution)
@@ -382,7 +382,7 @@ def calculate_fragment_length_distance(m
 
     nonreference_cdf = make_cdf(metrics['fragment_length_counts'])
 
-    diff = list(map(lambda x: x[0] - x[1], zip(nonreference_cdf, reference_cdf)))
+    diff = list([x[0] - x[1] for x in zip(nonreference_cdf, reference_cdf)])
     if diff:
         distance = (abs(max(diff)) > abs(min(diff))) and max(diff) or min(diff)
 
@@ -404,7 +404,7 @@ def add_fragment_length_distances(data,
 
         loaded_distribution = {}
         try:
-            loaded_distribution = {int(k): int(v) for k, v in json.load(open_maybe_gzipped(reference)).items()}
+            loaded_distribution = {int(k): int(v) for k, v in list(json.load(open_maybe_gzipped(reference)).items())}
         except:
             with open(reference, 'rb') as f:
                 dialect = csv.Sniffer().sniff(f.read(1024))
@@ -425,8 +425,8 @@ def add_fragment_length_distances(data,
             logging.warn('Reference distribution lacks value at fragment length {}; assigning zero'.format(fragment_length))
 
     # adjust the distribution to the requested maximum fragment length
-    cleaned_distribution = collections.defaultdict(long)
-    for fragment_length, [count, fraction_of_all_reads] in fragment_length_reference['distribution'].items():
+    cleaned_distribution = collections.defaultdict(int)
+    for fragment_length, [count, fraction_of_all_reads] in list(fragment_length_reference['distribution'].items()):
         if fragment_length <= max_fragment_length:
             cleaned_distribution[fragment_length] = count
 
@@ -443,15 +443,15 @@ def add_fragment_length_distances(data,
 
 def calculate_reference_peak_metrics(data):
     reference_peak_metrics = {
-        'source': 'calculated from ' + ', '.join(sorted([m['name'] for m in data['metrics'].values()])),
+        'source': 'calculated from ' + ', '.join(sorted([m['name'] for m in list(data['metrics'].values())])),
         'cumulative_fraction_of_hqaa': [],
         'cumulative_fraction_of_territory': []
     }
-    cfh = zip(*[metrics['peak_percentiles']['cumulative_fraction_of_hqaa'] for name, metrics in sorted(data['metrics'].items())])
-    reference_peak_metrics['cumulative_fraction_of_hqaa'] = map(lambda x: sum(x) / len(x), cfh)
+    cfh = list(zip(*[metrics['peak_percentiles']['cumulative_fraction_of_hqaa'] for name, metrics in sorted(data['metrics'].items())]))
+    reference_peak_metrics['cumulative_fraction_of_hqaa'] = [sum(x) / len(x) for x in cfh]
 
-    cft = zip(*[metrics['peak_percentiles']['cumulative_fraction_of_territory'] for name, metrics in sorted(data['metrics'].items())])
-    reference_peak_metrics['cumulative_fraction_of_territory'] = map(lambda x: sum(x) / len(x), cft)
+    cft = list(zip(*[metrics['peak_percentiles']['cumulative_fraction_of_territory'] for name, metrics in sorted(data['metrics'].items())]))
+    reference_peak_metrics['cumulative_fraction_of_territory'] = [sum(x) / len(x) for x in cft]
 
     return reference_peak_metrics
 
@@ -512,12 +512,12 @@ PERCENTAGES = {
 
 
 def prepare_for_viewer(data):
-    for name, metrics in data.items():
+    for name, metrics in list(data.items()):
         del metrics['peaks']
         del metrics['peaks_fields']
 
         metrics['percentages'] = {}
-        for numerator, denominator in PERCENTAGES.items():
+        for numerator, denominator in list(PERCENTAGES.items()):
             key = '{}__{}'.format(numerator, denominator)
             if metrics[denominator] == 0:
                 if metrics[numerator] == 0:
--- a/src/scripts/srvarv
+++ b/src/scripts/srvarv
@@ -1,4 +1,4 @@
-#!/usr/bin/env python
+#!/usr/bin/python3
 
 import argparse
 import os
@@ -7,8 +7,8 @@ try:
     import http.server as httpserver
     import socketserver
 except:
-    import SimpleHTTPServer as httpserver
-    import SocketServer as socketserver
+    import http.server as httpserver
+    import socketserver as socketserver
 
 
 if __name__ == '__main__':
@@ -28,7 +28,7 @@ if __name__ == '__main__':
     instance = os.path.abspath(args.instance)
     os.chdir(args.instance)
 
-    print('Serving ataqv web viewer instance from {} at http://localhost:{}'.format(instance, args.port))
+    print(('Serving ataqv web viewer instance from {} at http://localhost:{}'.format(instance, args.port)))
     try:
         httpd.serve_forever()
     except KeyboardInterrupt:
